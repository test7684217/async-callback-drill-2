const cardData = require("./cards_2.json");

function getCards(listID, callback) {
  try {
    setTimeout(() => {
      callback(cardData[listID]);
    }, 2 * 1000);
    
  } catch (error) {
    console.log(error);
  }
}

module.exports = getCards;
