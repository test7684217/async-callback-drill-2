const getBoard = require("./callback1.cjs");
const getList = require("./callback2.cjs");
const getCards = require("./callback3.cjs");

function callback5() {
  try {
    getBoard("mcu453ed", (board) => {
      console.log(board);
      getList(board.id, (lists) => {
        console.log(lists);
        const mindList = lists.find((list) => {
          return list.name === "Mind";
        });
        const spaceList = lists.find((list) => {
          return list.name === "Space";
        });

        getCards(mindList.id, (cards) => {
          console.log(cards);
        });

        getCards(spaceList.id, (cards) => {
          console.log(board);
          console.log(lists);
          console.log(cards);
        });
      });
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports = callback5;
