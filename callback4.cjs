const getBoard = require("./callback1.cjs");
const getList = require("./callback2.cjs");
const getCards = require("./callback3.cjs");

function callback4() {
  try {
    getBoard("mcu453ed", (board) => {
      getList(board.id, (lists) => {
        const mindList = lists.find((list) => {
          return list.name === "Mind";
        });

        getCards(mindList.id, (cards) => {
          console.log(board);
          console.log(lists);
          console.log(cards);
        });
      });
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports = callback4;
