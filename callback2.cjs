const listData = require("./lists_1.json");

function getList(boardID, callback) {
  try {
    setTimeout(() => {
      const list = listData[boardID];
      callback(list);
    }, 2 * 1000);
  } catch (error) {
    console.log(error);
  }
}

module.exports = getList;
