const getBoard = require("./callback1.cjs");
const getList = require("./callback2.cjs");
const getCards = require("./callback3.cjs");

function callback6() {
  try {
    getBoard("mcu453ed", (board) => {
      console.log(board);
      getList(board.id, (list) => {
        console.log(list);
        list.forEach((card) => {
          getCards(card.id, (data) => {
            if(data) {
              console.log(data);
            }
          });
        });
      });
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports = callback6;
