const boardData = require("./boards_2.json");

function getBoard(boardID, callback) {
  try {
    setTimeout(() => {
      let result = boardData.find((board) => {
        if (board.id === boardID) {
          return board;
        }
      });

      callback(result);
    }, 2 * 1000);
  } catch (error) {
    console.log(error);
  }
}

module.exports = getBoard;
